<?php
require_once 'Config.php';

class DataBase
{
    private static $_instence = false;
    private static $_pdo = false;

    private function __construct()
    {
        //Creamos la conexion con el servidor
        try {
            self::$_pdo = new PDO('mysql:host=' . Config::$dbServidor . ';dbname=' . Config::$dbBasedatos . ';charset=utf8', Config::$dbUser, Config::$dbPasswd);
            self::$_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $error) {
            die("<b>Error MYSQL: </b>" . $error->getMessage());
        }
    }

    public static function getInstence()
    {
        //Comprueba si tenemos una instancia de la clase basedatos, si es asi devuelve el objeto pdo,
        //si no hay instancia crea un objeto de la clase basedatos.

        if (!self::$_instence instanceof self) {
            self::$_instence = new self();
        }
        return self::$_pdo;
    }

    private function __clone()
    {
        trigger_error('Clonacion de este objeto no permitida', E_USER_ERROR);
    }
}

?>