<?php
require_once(__DIR__ . '/php/autoloader.php');

/**
 * Created by PhpStorm.
 * User: Marco
 * Date: 24/05/2016
 * Time: 21:51
 */
class Telegram
{
    private static $_web_site = "https://api.telegram.org/bot";
    private $url;
    private $chat_id;
    private $location;
    private $name;
    private $text;

    function __construct($token)
    {
        $this->url = self::$_web_site . $token;
    }

    function setWebhook($user_url)
    {
        file_get_contents($this->url . "/setwebhook?url=" . $user_url);
    }

    function sendMessage($message)
    {
        file_get_contents($this->url . "/sendmessage?chat_id=" . $this->chat_id . "&text=" . urlencode($message) . "&parse_mode=HTML");
    }

    function sendPhoto($foto, $caption = "")
    {
        $datos = array(
            "chat_id" => $this->chat_id,
            "photo" => new CURLFile(realpath($foto)),
            "caption" => $caption
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content/Type:multipart/form-data"));
        curl_setopt($curl, CURLOPT_URL, $this->url . "/sendphoto");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datos);
        curl_exec($curl);
        curl_close($curl);
    }

    function sendAudio($audio)
    {
        $datos = array(
            "chat_id" => $this->chat_id,
            "audio" => new CURLFile(realpath($audio))
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content/Type:multipart/form-data"));
        curl_setopt($curl, CURLOPT_URL, $this->url . "/sendaudio");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datos);
        curl_exec($curl);
        curl_close($curl);
    }

    function sendWebcam()
    {
        $foto = array("http://85.91.64.26/quintana/readImage.asp", "http://85.91.64.19/praterias/readImage.asp", "http://85.91.64.26/obradoiro/readImage.asp", "http://85.91.64.19/catedralc/readImage.asp", "http://85.91.64.19/santiago/readImage.asp");
        $fotoAleatoria = array_rand($foto);

        file_put_contents('./resources/foto.jpg', file_get_contents($foto[$fotoAleatoria]));

        $rutaRealFichero = realpath("./resources/foto.jpg");
        $datos = array(
            'chat_id' => $this->chat_id,
            'photo' => new CURLFile($rutaRealFichero)
        );
        print_r($rutaRealFichero);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content/Type:multipart/form-data"));
        curl_setopt($curl, CURLOPT_URL, $this->url . "/sendphoto");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datos);
        curl_exec($curl);
        curl_close($curl);;
    }

    function sendVideo($video)
    {
        $datos = array(
            "chat_id" => $this->chat_id,
            "video" => new CURLFile(realpath($video))
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content/Type:multipart/form-data"));
        curl_setopt($curl, CURLOPT_URL, $this->url . "/sendvideo");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datos);
        curl_exec($curl);
        curl_close($curl);
    }


    function sendLocation($latitude, $longitude)
    {
        file_get_contents($this->url . "/sendlocation?chat_id=" . $this->chat_id . "&latitude=$latitude&longitude=$longitude");
    }

    function sendCalle()
    {

        $btn = new stdClass();
        $btn->text = "Enviar Mi Localización";
        $btn->request_location = true;

        $teclado = array(
            'keyboard' => [[$btn]],
            'one_time_keyboard' => true
        );

        $teclado = json_encode($teclado);
        $datos = array(
            'chat_id' => $this->chat_id,
            'text' => "Envíame tu ubicación!",
            'parse_mode' => 'HTML',
            'reply_markup' => $teclado
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content/Type:multipart/form-data"));
        curl_setopt($curl, CURLOPT_URL, $this->url . "/sendMessage");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datos);
        curl_exec($curl);
        curl_close($curl);;
    }

    function sendKeyboard($data, $text)
    {

        $keyboard = json_encode($data);

        $post = array(
            'chat_id' => $this->chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'reply_markup' => $keyboard
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url . "/sendmessage");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($curl);
        curl_close($curl);
    }

    function getUpdates()
    {
        //$updates=file_get_contents('php://input');
        $updates = file_get_contents($this->url . '/getupdates');
        $updates_array = json_decode($updates, true);
        $lastupdate = count($updates_array['result']) - 1;
        $this->chat_id = $updates_array['result'][$lastupdate]["message"]["chat"]["id"];
        $this->name = $updates_array['result'][$lastupdate]["message"]["from"]["first_name"];
        $this->text = $updates_array['result'][$lastupdate]["message"]["text"];
        // $this->location = $updates_array['result'][0]["message"]["location"];

        return $updates;
    }

    /*
     *GETTERS AND SETTERS
     */
    function getChatId()
    {
        return $this->chat_id;
    }

    function getText()
    {
        return $this->text;
    }

    function getUser()
    {
        return $this->name;
    }

    function getLocation()
    {
        return $this->location;
    }

    function setChatId($chatId)
    {
        $this->chat_id = $chatId;
    }
}