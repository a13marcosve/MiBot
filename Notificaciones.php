<?php

include 'DataBase.php';
include 'telegram.php';
include 'php/autoloader.php';

$bot_token = '234777407:AAHTvAfZ_qFXRsJL6AYugyj6M-5-0Qd_aj4';
$meteo_url = 'http://api.wunderground.com/api/0478a609b8db3141/forecast/geolookup/conditions/lang:SP/q';

$pdo = DataBase::getInstence();
$bot = new Telegram(BOT_TOKEN);
try {
    $qry = $pdo->prepare("select * from avisos where meteo = true or titulares = true");
    $qry->execute();
    $qry->setFetchMode(PDO::FETCH_ASSOC);
    while ($res = $qry->fetch()) {
        $bot->setChatId($res['chatId']);
        if ($res['meteo'] && $res['hora_meteo'] == date("H:i")) {
            $response = json_decode(file_get_contents($meteo_url . "/SPAIN/A_CORUNA.json"));
            foreach ($response->forecast->txt_forecast->forecastday as $dia) {
                if ($dia->period % 2 === 0) {
                    file_put_contents('foto.jpg', file_get_contents($dia->icon_url));
                    $bot->sendPhoto('foto.jpg', $dia->title . "\n" . $dia->fcttext_metric . "\n\n");
                }
            }
        }
        if ($res['titulares'] && $res['hora_titulares'] == date("H:i")) {
            $bot->sendMessage(getNews('http://ep00.epimg.net/rss/tags/ultimas_noticias.xml'));
        }
    }
} catch (PDOException $e) {
    echo "Ha ocurrido un erro: " . $e->getMessage();
}


function getNews($url)
{
    $feed = new SimplePie();
    $feed->set_feed_url($url);
    $feed->init();
    $result = "<b>" . $feed->get_title() . "</b>\n";
    $noticias = $feed->get_items();
    for ($i = 0; $i < sizeof($noticias); $i++) {
        $result = $result . "\n" . $noticias[$i]->get_title();
        $result = $result . "\n" . $noticias[$i]->get_link() . "\n";
    }
    return $result;
}