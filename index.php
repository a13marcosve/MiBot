<?php
include 'telegram.php';
include 'php/autoloader.php';
include 'dataBase.php';

//https://api.factual.com/geotag?latitude=[latitude]&longitude=[longitude]&KEY=[your API key]

$bot_token = '234777407:AAHTvAfZ_qFXRsJL6AYugyj6M-5-0Qd_aj4';
$geo_token = 'oCz3S7YTY1GtREx57xFTWUEAuCZhVxC2SZhHCnZ4';

$bot = new Telegram($bot_token);

$updates = $bot->getUpdates();
$last_text = $bot->getText();
//$pdo = Basedatos::getInstancia();
switch ($last_text) {
    case '/start':
        $bot->sendMessage("
        /start :Ayuda
	  /hora :Hora actual
	  /cancion :Canción al azar.
	  /aeropuerto :Coordenadas del Aeropuerto de Santiago
	  /webcam :Archivo multimedia.
	  /mytube :Archivo de video.
	  /calle :Geolocalizacion.
	  /elpais :Titulares de El Pais.
	  /avisos :Enviará automáticamente notificaciones de el tiempo y titulares de El país");
        break;

    case '/hora':
        $message = date("H:i:s");
        $bot->sendMessage($message);
        break;

    case '/cancion':
        $rand = [rand(0, count($list) - 1)];
        $bot->sendAudio("./resources/" . getMedia('./resources', 'mp3'), $rand);
        break;

    case '/aeropuerto':
        $bot->sendLocation(42.896389, -8.415278);
        break;

    case '/webcam':
        $bot->sendWebcam();
        break;

    case '/mytube':
        $bot->sendVideo("./resources/" . getMedia('./resources', 'mp4')[0]);
        break;

    case '/calle':
        $bot->sendCalle();

        break;

    case '/avisos':
        $notify_keyboard = array(
            'keyboard' => [["\xE2\x9B\x85 El tiempo", "\xF0\x9F\x93\xB0 El pais"], ["\xF0\x9F\x9A\xAB Desactivar meteo", "\xF0\x9F\x9A\xAB Desactivar noticias"]],
            'one_time_keyboard' => true
        );

        $bot->sendKeyboard($notify_keyboard, "Configurar avisos");
        break;

    case "\xE2\x9B\x85 El tiempo":
        $hour_keyboard = array(
            'keyboard' => [["\xE2\x9B\x85 07:00", "\xE2\x9B\x85 08:00", "\xE2\x9B\x85 09:00", "\xE2\x9B\x85 10:00"], ["\xE2\x9B\x85 11:00", "\xE2\x9B\x85 12:00", "\xE2\x9B\x85 13:00", "\xE2\x9B\x85 14:00"], ["\xE2\x9B\x85 15:00", "\xE2\x9B\x85 16:00", "\xE2\x9B\x85 17:00", "\xE2\x9B\x85 18:00"], ["\xE2\x9B\x85 19:00", "\xE2\x9B\x85 20:00", "\xE2\x9B\x85 21:00", "\xE2\x9B\x85 22:00"]],
            'one_time_keyboard' => true
        );

        $bot->sendKeyboard($hour_keyboard, "Escoge una hora");
        break;

    case "\xF0\x9F\x93\xB0 El pais":
        $hour_keyboard = array(
            'keyboard' => [["\xF0\x9F\x93\xB0 07:00", "\xF0\x9F\x93\xB0 08:00", "\xF0\x9F\x93\xB0 09:00", "\xF0\x9F\x93\xB0 10:00"], ["\xF0\x9F\x93\xB0 11:00", "\xF0\x9F\x93\xB0 12:00", "\xF0\x9F\x93\xB0 13:00", "\xF0\x9F\x93\xB0 14:00"], ["\xF0\x9F\x93\xB0 15:00", "\xF0\x9F\x93\xB0 16:00", "\xF0\x9F\x93\xB0 17:00", "\xF0\x9F\x93\xB0 18:00"], ["\xF0\x9F\x93\xB0 19:00", "\xF0\x9F\x93\xB0 20:00", "\xF0\x9F\x93\xB0 21:00", "\xF0\x9F\x93\xB0 22:00"]],
            'one_time_keyboard' => true
        );

        $bot->sendKeyboard($hour_keyboard, "Escoge una hora");
        break;

    case 'Desactivar avisos':
        $bot->sendMessage("Todos los avisos desactivados");
        break;

    case '/elpais':
        $bot->sendMessage(getNews('http://ep00.epimg.net/rss/tags/ultimas_noticias.xml'));
        break;

    default:
        if (preg_match("/\\xE2\\x9B\\x85.*:00/", $bot->getText())) {
            try {
                $hora = explode(" ", $bot->getText())[1];
                $qry = $pdo->prepare("select * from avisos where chatId = ?");
                $qry->bindParam(1, $bot->getChatId());
                $qry->execute();
                if ($qry->rowCount() != 0) {
                    $qry = $pdo->prepare("update avisos set hora_meteo = ?, meteo = true where chatId = ?");
                    $qry->bindParam(1, $hora);
                    $qry->bindParam(2, $bot->getChatId());
                    $qry->execute();
                    $bot->sendMessage("Genial " . $bot->getUser() . ".\nLa prediccion se enviara a las " . $hora . " horas");
                } else {
                    $qry = $pdo->prepare("insert into avisos(chatId,meteo,hora_meteo,titulares,hora_titulares) values(?,true,?,false,null)");
                    $qry->bindParam(1, $bot->getChatId());
                    $qry->bindParam(2, $hora);
                    $qry->execute();
                    $bot->sendMessage("Configuracion guardada para las " . $hora . " horas.");
                }
            } catch (PDOException $e) {
                $bot->sendMessage("Ha ocurrido un error, no se ha guardado su configuracion");
            }
        } elseif (preg_match("/\\xF0\\x9F\\x93\\xB0.*:00/", $bot->getText())) {
            // COnfiguracion de el pais
            try {
                $hora = explode(" ", $bot->getText())[1];
                $qry = $pdo->prepare("select * from avisos where chatId = ?");
                $qry->bindParam(1, $bot->getChatId());
                $qry->execute();
                if ($qry->rowCount() != 0) {
                    $qry = $pdo->prepare("update avisos set hora_titulares = ?, titulares = true where chatId = ?");
                    $qry->bindParam(1, $hora);
                    $qry->bindParam(2, $bot->getChatId());
                    $qry->execute();
                    $bot->sendMessage("Perfecto " . $bot->getUser() . ".\nRecibiras los titulares a las " . $hora . ".");
                } else {
                    $qry = $pdo->prepare("insert into avisos(chatId,meteo,hora_meteo,titulares,hora_titulares) values(?,false,null,true,?)");
                    $qry->bindParam(1, $bot->getChatId());
                    $qry->bindParam(2, $hora);
                    $qry->execute();
                    $bot->sendMessage("Configuracion guardada para las " . $hora . " horas.");
                }
            } catch (PDOException $e) {
                $bot->sendMessage("ERRO");
            }
        } elseif (preg_match("/\\xF0\\x9F\\x9A\\xAB Desactivar noticias/", $bot->getText())) {
            try {
                $hora = explode(" ", $bot->getText())[1];
                $qry = $pdo->prepare("select * from avisos where chatId = ?");
                $qry->bindParam(1, $bot->getChatId());
                $qry->execute();
                if ($qry->rowCount() != 0) {
                    $qry = $pdo->prepare("update avisos set titulares = false where chatId = ?");
                    $qry->bindParam(1, $bot->getChatId());
                    $qry->execute();
                    $bot->sendMessage("Las noticias han sido desactivadas");
                } else {
                    $bot->sendMessage("Las noticias ya estan desactivadas");
                }
            } catch (PDOException $e) {
                $bot->sendMessage("ERRO");
            }
        } elseif (preg_match("/\\xF0\\x9F\\x9A\\xAB Desactivar meteo/", $bot->getText())) {
            try {
                $hora = explode(" ", $bot->getText())[1];
                $qry = $pdo->prepare("select * from avisos where chatId = ?");
                $qry->bindParam(1, $bot->getChatId());
                $qry->execute();
                if ($qry->rowCount() != 0) {
                    $qry = $pdo->prepare("update avisos set meteo = false where chatId = ?");
                    $qry->bindParam(1, $bot->getChatId());
                    $qry->execute();
                    $bot->sendMessage("Las prediccion meteorologica ha sido desactivada");
                } else {
                    $bot->sendMessage("Las prediccion meteorologica ya esta desactivada");
                }
            } catch (PDOException $e) {
                $bot->sendMessage("ERRO");
            }
        } elseif (!$bot->getLocation()) {
            $bot->sendMessage("/start Ayuda del Bot\n/hora Envia hora actual\n/cancion Enviará 1 canción al azar.\n/aeropuerto Enviará las coordenadas del Aeropuerto de Santiago\n/webcam Enviará 1 foto al azar indicando de que webcam se trata\n/mytube Enviará un video que tengamos almacenado en el servidor.\n/calle Devolverá el nombre de la calle, código postal, etc.. de dónde nos encontremos.\n/elpais Mostrará los últimos Titulares de El Pais\n/avisos Enviará automáticamente notificaciones de el tiempo y titulares de El país");
        }
        break;
}

function getMedia($dir, $type)
{
    $list = array();
    if (is_dir($dir)) {
        if ($filedir = opendir($dir)) {
            while ($file = readdir($filedir)) {
                $ext = explode('.', $file);
                if ($ext[1] === $type) {
                    array_push($list, $file);
                }
            }
        }
    }
    return $list;
}

function getNews($url)
{
    $feed = new SimplePie();
    $feed->set_feed_url($url);
    $feed->init();
    $result = "<b>" . $feed->get_title() . "</b>\n";
    $noticias = $feed->get_items();
    for ($i = 0; $i < sizeof($noticias); $i++) {
        $result = $result . "\n" . $noticias[$i]->get_title();
        $result = $result . "\n" . $noticias[$i]->get_link() . "\n";
    }
    return $result;
}

if ($bot->getLocation()) {
    $info = json_decode(file_get_contents('https://api.factual.com/geotag?latitude=' . $bot->getLocation()['latitude'] . '&longitude=' . $bot->getLocation()['longitude'] . '&KEY=' . $geo_token));
    $info = $info->response->data;

    $bot->sendMessage("Calle: <i>" . $info->street_name->name . "</i>\nCiudad: <i>");
}
